/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package sample;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class AppMain {
    public static void main(String[] args) throws Exception{
        new AppMain().doMain();
    }

    public  void doMain() throws Exception{
        for(Method m : Arrays.asList(Hoge.class.getDeclaredMethods())){
            System.out.printf("** method: %s.\n", m.getName());
            for(Parameter param : m.getParameters()){
                System.out.printf("   %s: %s, %s.\n", param.getName(), param.getType(), param.getParameterizedType());
            }
        }

        new Hoge("Hello").say("HogeHoge");
        new Hoge("Hello").say(new StringBuffer("FugaFuga"));
        new Hoge("Hello").say((Object)"FizzBuzz");
    }
}
