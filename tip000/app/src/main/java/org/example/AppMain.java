package org.example;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

/**
 * リフレクションを確認する
 */
public class AppMain {

    public static void main(String[] args) throws Exception, Throwable {
        new AppMain().doMain();
    }

    public void doMain() throws Exception, Throwable  {
        String s1 = test01("hoge");
        Integer i1 = test01(10);
    }

    String test01(String v){
        System.out.printf("** test01(String): v=[%s].\n", v);
        return v+v;
    }

    <T extends Integer> T test01(T v){
        System.out.printf("** test01(T<Integer): v=[%s].\n", v);
        Integer i = Integer.valueOf(v);
        //Ng: return (T)(i+i);    //  error: incompatible types: int cannot be converted to T
        return (T)Integer.valueOf(i+i);    // OK: 
    }

    <T> T test01(Object v){
        System.out.printf("** test01(T<String): v=[%s].\n", v);
        return (T)v;
    }

}
