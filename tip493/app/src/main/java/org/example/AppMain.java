package org.example;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.MethodHandle;

public class AppMain {

    public static void main(String[] args) throws Exception, Throwable {
        new AppMain().doMain();
    }

    public void doMain() throws Exception, Throwable  {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.methodType(Integer.class, String.class);
        MethodHandle mh = lookup.findStatic(Integer.class, "valueOf", methodType);
        //NG: error: incompatible types: Object cannot be converted to Integer
        // Integer i = mh.invoke("123");
        //OK: Integer i = Integer.class.cast(mh.invoke("123"));
        //OK: 
        Integer i = (Integer)mh.invoke("123");
        //NG: java.lang.ClassCastException: Cannot cast [Ljava.lang.String; to java.lang.String
        // Integer i = (Integer)mh.invoke(new String[]{"123"});
        System.out.printf("** i=[%s].\n", i);
    }

}
