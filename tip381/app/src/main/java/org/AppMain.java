package org;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class AppMain {
    public static void main(String[] args) {
        new AppMain().run(args);
    }

    public void run(String[] args){
        List<List<String>> list = new ArrayList<>(Arrays.asList(
            Arrays.asList("Tokyo", "Chiba"),
            Arrays.asList("Saitama", "Kanagawa", "Gunma"),
            Arrays.asList("Tochigi", "Ibaraki", "Yamanashi")
        ));
 
        System.out.println("------元のlistの内容------");
        list.stream()
            .forEach(l -> System.out.println(l));
 
        System.out.println("");
        System.out.println("------flatMapにより纏めた結果------");
        list.stream()
            .flatMap(l -> l.stream())
            .forEach(e -> System.out.println(e));
    }
}
