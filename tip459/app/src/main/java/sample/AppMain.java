/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package sample;

import java.lang.reflect.Constructor;

public class AppMain {
    public static void main(String[] args) throws Exception{
        new AppMain().doMain();
    }

    public  void doMain() throws Exception{
        new Hoge().say();
        new Hoge("hello").say();

        Hoge.class.getConstructor().newInstance().say();
        Hoge.class.getConstructor(String.class).newInstance("Hi").say();
        Hoge.class.getConstructor("huga".getClass()).newInstance("Good morning").say();
    }
}
