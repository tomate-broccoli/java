package org.example;

/**
 *  Object->Number-+->Integer
 *                 +->Long
 */
public class AppMain1 {

    public static void main(String[] args) throws Exception, Throwable {
        new AppMain().doMain();
    }

    public void doMain() throws Exception, Throwable  {
        test01(1);           //->01
        test01(1L);          //->03   (02 if test01(Long) exists)
        test01((Long)1L);      //->03   (02 if test01(Long) exists)
        test01((Number)1L);    //->03
        // test01((Long)1);    //->error: incompatible types: int cannot be converted to Long
        // test01((Long)((Number)1));    //->java.lang.ClassCastException: class java.lang.Integer cannot be cast to class java.lang.Long (java.lang.Integer and java.lang.Long are in module java.base of loader 'bootstrap')
    }

    void test01(Integer v){
        System.out.printf("** 01: test01(Integer): v=[%s].\n", v);
    }
/*
    void test01(Long v){
        System.out.printf("** 02: test01(Long): v=[%s].\n", v);
    }
*/

    void test01(Number v){
        System.out.printf("** 03: test01(Number): v=[%s].\n", v);
    }

}
