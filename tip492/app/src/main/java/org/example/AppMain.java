package org.example;

/**
 *  Object->Number-+->Integer
 *                 +->Long
 */
public class AppMain {

    public static void main(String[] args) throws Exception, Throwable {
        new AppMain().doMain();
    }

    public void doMain() throws Exception, Throwable  {
        Number n;
        Integer i = 1;
        n = i;
        // i = n;          // error: incompatible types: Number cannot be converted to Integer
        i = (Integer)n;    // OK:

        Long l = 1L;
        n = l;
        // l = n;          // error: incompatible types: Number cannot be converted to Long
        l = (Long)n;       // OK:

        // l = i;          // error: incompatible types: Integer cannot be converted to Long
        // l = (Long)i;    // error: incompatible types: Integer cannot be converted to Long
    }

}
