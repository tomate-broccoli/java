package sample;

import java.util.List;
import org.mapstruct.factory.Mappers;
import java.lang.reflect.Method;

public class CommonAbstract<M, E, D> {
    M mapper;

    public CommonAbstract(M mapper){
        this.mapper = mapper;
    }

    public List<E> from(List<D> dtoList) throws Exception{
        Method method = null;
        Method[] methods = mapper.getClass().getDeclaredMethods();
        for(int i=0; i<methods.length; i++){
            method = methods[i];
            System.out.printf("*** method=[%s].\n", method.getName());
            if(method.getName().equals("from")) break;
        }
        return (List<E>) method.invoke(mapper, dtoList);
    }

}
