package sample;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper
public interface HogeMapper {
    List<HogeEntity> from(List<HogeDto> list);
}
