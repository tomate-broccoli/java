package sample;

import java.util.List;
import org.mapstruct.factory.Mappers;

public class HogeAbstract {
    HogeMapper mapper;

    public List<HogeEntity> from(List<HogeDto> DtoList){
        mapper = Mappers.getMapper(HogeMapper.class);
        return mapper.from(DtoList);
    }
}
