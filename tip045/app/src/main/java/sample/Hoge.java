package sample;

import java.util.ArrayList;
import java.util.List;
import org.mapstruct.factory.Mappers;

// public class Hoge extends HogeAbstract {
public class Hoge extends CommonAbstract<HogeMapper, HogeEntity, HogeDto> {

    public Hoge(){
        super(Mappers.getMapper(HogeMapper.class));
    }

    List<HogeDto> list =null;

    public void run() throws Exception{
        List<HogeDto> list = new ArrayList<HogeDto>(){{
            add(new HogeDto(1, "abc"));
            add(new HogeDto(2, "xyz"));
        }};
        for(HogeEntity e : from(list)){
            System.out.println(e);
        }
    }
}
