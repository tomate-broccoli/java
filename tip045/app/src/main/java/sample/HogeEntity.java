package sample;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HogeEntity{
    private int id;
    private String name;
}
