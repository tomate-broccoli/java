package sample;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HogeDto {
    private int id;
    private String name;
}
