package sample;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-11T11:07:45+0000",
    comments = "version: 1.5.5.Final, compiler: Eclipse JDT (IDE) 3.38.0.v20240417-1011, environment: Java 17.0.11 (Azul Systems, Inc.)"
)
public class HogeMapperImpl implements HogeMapper {

    @Override
    public List<HogeEntity> from(List<HogeDto> list) {
        if ( list == null ) {
            return null;
        }

        List<HogeEntity> list1 = new ArrayList<HogeEntity>( list.size() );
        for ( HogeDto hogeDto : list ) {
            list1.add( hogeDtoToHogeEntity( hogeDto ) );
        }

        return list1;
    }

    protected HogeEntity hogeDtoToHogeEntity(HogeDto hogeDto) {
        if ( hogeDto == null ) {
            return null;
        }

        int id = 0;
        String name = null;

        id = hogeDto.getId();
        name = hogeDto.getName();

        HogeEntity hogeEntity = new HogeEntity( id, name );

        return hogeEntity;
    }
}
