package org.example;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.MethodHandle;
import java.util.List;
import java.lang.reflect.Method;
import java.util.Arrays;

public class AppMain {

    public static void main(String[] args) throws Exception, Throwable {
        new AppMain().doMain();
    }

    public void doMain() throws Exception, Throwable  {
        Method m = Integer.class.getDeclaredMethod("valueOf", String.class);
        System.out.printf("** m.getParameterTypes().length=[%s].\n", m.getParameterTypes().length);    // ** m.getParameterTypes().length=[1].
        Arrays.stream(m.getParameterTypes()).forEach(System.out::println);                                    // class java.lang.String

        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.methodType(Integer.class, m.getParameterTypes());
        MethodHandle mh = lookup.findStatic(Integer.class, "valueOf", methodType);

        Integer i = (Integer)mh.invoke("123");
        System.out.printf("** i=[%s].\n", i);
    }

}
