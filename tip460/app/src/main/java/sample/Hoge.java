/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package sample;

public class Hoge {
    String msg = "Bye";

    public Hoge(){
        // nothing.
    }
    public Hoge(String msg){
        this.msg = msg;
    }

    public void say(){
        System.out.printf("*** %s Hoge.\n", this.msg);
    }
    public void say(String name){
        System.out.printf("*** %s %s.\n", this.msg, name);
    }
    public <T> void say(T name){
        System.out.printf("*** %s %s!\n", this.msg, name);
    }
}
